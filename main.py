#!/usr/bin/python3
import os
import argparse
import init
import config
import configparser

# Globals
HOME = os.environ['HOME']
CONFIG = HOME + "/.backup.conf"
KEY = ""
BACKUP_IP = ""
USER = ""
DEST_DIR = "backup"

###########################################
# Options parser
parser = argparse.ArgumentParser(prog='SMA(Save My Ass)')
parser.add_argument('-v', '--verbose', dest="verbose", help='Turn verbosity on', default=False, action="store_true")
parser.add_argument('-c', '--config', dest="config", help='Config file to use', metavar="File", default=CONFIG)
parser.add_argument('-i', '--ip ', dest="BACKUP_IP", help="What ip to backup to", metavar="IP", default=BACKUP_IP)
parser.add_argument('-r', '--reconfigure ', dest="reconfigure", help="Reconfigure config", default=False, action="store_true")
parser.add_argument('--destination', dest="DEST_DIR", help="Remote directory to backup to", metavar="<Remote folder>", default=DEST_DIR)
parser.add_argument('-d', '--directory', dest="directory", help='Local directroy to backup', metavar="<Local Folder>", required=True)
arguments = parser.parse_args()

VERBOSE = arguments.verbose
RECONFIGURE = arguments.reconfigure
BACKUP_IP = arguments.BACKUP_IP
directory = arguments.directory
DEST_DIR = arguments.DEST_DIR
CONFIG = str(arguments.config)
##########################################

def reconfigure():
    if RECONFIGURE:
        config.main()

####################################
# Config file
if not os.path.isfile(HOME + "/.backup.ini"):
    config.main()

reconfigure()

conf = configparser.ConfigParser()
conf.read(HOME + '/.backup.ini')
KEY = conf['BACKUP']['KeyPath']
# Checking for option -i
if BACKUP_IP:
    BACKUP_IP = BACKUP_IP.strip()
else:
    BACKUP_IP = conf['BACKUP']['Ip']
USER = conf['BACKUP']['user']
PORT = conf['BACKUP']['port']
#####################################


def verbose(text):
    if VERBOSE:
        print(text)

def transfer(folder):
    # TODO Create visuals
    print("Backing up " + directory + " To " + USER + "@" + BACKUP_IP)
    # Checking/creating backup folder
    os.system("ssh -i " + KEY + " -p " + PORT + " " + USER + "@" + BACKUP_IP + " mkdir -p " + DEST_DIR)
    # Syncing files
    os.system("rsync -azz -e 'ssh -i "+ KEY + " -p " + PORT + "' " + directory + " " + USER + "@" + BACKUP_IP + ":" + DEST_DIR)

def main():
    transfer(directory)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nInterrupted\n")
