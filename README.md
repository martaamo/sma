# SMA 
SMA is an easy way to back up directories to an external server
```bash
python main.py -d <folder>
```

The program wil ask for remote server IP, ssh key path and remote user(name)

You can either run the program as an alias or compile it and install the binary to /bin/backup

## Alias
Create alias in either .zshrc or .bashrc
```bash
alias backup = 'python <path to source repo>/main.py' 
```
Usage after alias is added:
```bash 
backup -d <directory to backup>
```

## Compile and install binary
To compile the program into one binary called backup just run the install.sh script
Though this requires sudo. To install pyinstaller with pip and to copy to /bin
```bash
./install.sh
```
Usage after install:
```bash
backup -d <directory to backup>
```

## Update
If using the binary method
```bash
cd <SMA repo>
git pull
./install.sh
```
If using the alias method
```bash
git pull
```

### Requirements
* Python
* pip
* pyinstaller(installed by the install script)
* ssh/rsync(should be included in every distro)


