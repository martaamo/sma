#!/bin/bash
# Tell the user that sudo is required and that this has some risk
printf "<#- Sudo required. Press 'y' if you want to cancel install\n"
# Check if user wants to continue
read -p '<#- Cancel(y/n): ' ans
if [ "$ans" == 'y' ]
then
  exit
fi
printf "<#- Installing pyinstaller\n"
# Install pyinstaller and print output to /dev/null, errors aswell
pip3 install --user pyinstaller > /dev/null 2>&1
printf "<#- Compiling main.py to binary called backup\n"
# Compile main.py to binary called backup and print output to /dev/null, errors aswell
pyinstaller --onefile main.py > /dev/null 2>&1
printf "<#- Copying backup to /bin\n\n"
# Copy the binary to /bin
sudo cp dist/main /bin/backup
# Check if the binary is in /bin
sudo ls /bin/backup > /dev/null 2>&1
if [ $? == 0 ]
then
  printf "<#- Sucessful install\n"
else
  printf "<#- Unsucessful install\n"
fi
