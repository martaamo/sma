# v1.0.0, 11/09/19 

+ added version number
+ added changelog

# v1.0.1, 11/09/19 

+ using pip3
+ Installing pycharm to user, not root
+ Verison number

# v1.0.2, 11/09/19 

+ Bugfixed option -i

# v1.1.0, 11/09/19 

+ Added option '--destination' to chose destination directory

# v1.1.1, 11/09/19 

+ Versioning system now tested and working properly

# v1.1.2, 11/09/19

+ Made output nicer

# v1.1.3, 11/09/19

- Testing versioning.

# v1.2.0, 12/09/19

* More efficient parsing of config file
