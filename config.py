#!/usr/bin/python
import os
import configparser

HOME = os.environ['HOME']
KEY = ""
backup_ip = ""
user = ""
port = "22"

def createConfig():
    config = configparser.ConfigParser()
    # Input
    key = input("Which key will be used (Full path): ")
    backup_ip = input("What ip address will get the backup: ")
    user = input("What user will be used(remote): ")
    port = input("What port will be used(default 22): ")
    config['BACKUP'] = {'KeyPath':  key,
                        'Ip': backup_ip,
                        'user': user,
                        'port': port}
    with open(HOME + '/.backup.ini', 'w') as configfile:
        config.write(configfile)




def main():
    createConfig()
    print("\n")

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nInterrupted\n")


